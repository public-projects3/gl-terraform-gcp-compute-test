#!/bin/bash

# **Important** Both TFC_ORG and TFC_TOKEN environment variables should be set prior to running this script
# This script uploads a Sentinel policy to Terraform Cloud using the Policies API
# We assume the workspace name is my-gcp-gitlab-pipeline, please adjust this if needed
# We assume the TFC hostname name is app.terraform.io, please adjust this if needed
export TFC_ADDR="app.terraform.io"
if [ -z $TFC_WORKSPACE ]; then
  # Override default workspace name if needed
  export TFC_WORKSPACE="my-gcp-gitlab-pipeline"
  echo "Using default Workspace name: $TFC_WORKSPACE"
fi

# Validate that variables are set and obtain the workspace ID
./check_tfc_vars.sh
workspace_id=$(cat workspace_id)
# Check exit code
if [ -z "$workspace_id" ]
then
  echo "Could not get workspace ID, exitting."
  exit 
fi

# Create policy set
sed "s/my-workspace-id/${workspace_id}/" < ../api_templates/policysets.json.template > policysets.json
curl --header "Authorization: Bearer ${TFC_TOKEN}" \
 --header "Content-Type: application/vnd.api+json" \
 --request POST --data @policysets.json \
 "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/policy-sets" > policyset_result.json

policy_set_id=$(cat policyset_result.json | jq -r .data.id)
echo "Policy Set ID is ${policy_set_id}"

# Create policy set version
curl \
  --header "Authorization: Bearer $TFC_TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  https://${TFC_ADDR}/api/v2/policy-sets/${policy_set_id}/versions > policy-set-version.json

policy_set_version_id=$(cat policy-set-version.json | jq -r .data.id)
upload_url=$(cat policy-set-version.json | jq -r .data.links.upload)
echo "Policy set version id is ${policy_set_version_id}"
echo "Upload URL is ${upload_url}"

# Upload policy
rm -f myconfig.tar.gz
tar -cvf myconfig.tar *.sentinel sentinel.hcl
gzip myconfig.tar

curl -v \
  --header "Authorization: Bearer $TFC_TOKEN" \
  --request PUT \
  -F 'data=@myconfig.tar.gz' \
  "${upload_url}"

# Attach Policy Set to workspaces
rm -f payload.json
cat <<EOF > payload.json
{
  "data": [ { "id": "${workspace_id}", "type": "workspaces" } ]
}
EOF

curl \
  -H "Authorization: Bearer ${TFC_TOKEN}" \
  -H "Content-Type: application/vnd.api+json" \
  --request POST \
  --data @payload.json \
  "https://${TFC_ADDR}/api/v2/policy-sets/${policy_set_id}/relationships/workspaces"

# Check policy set version
curl \
  --header "Authorization: Bearer ${TFC_TOKEN}" \
  --request GET \
  "https://${TFC_ADDR}/api/v2/policy-set-versions/${policy_set_version_id}"
