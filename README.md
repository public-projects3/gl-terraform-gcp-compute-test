# Terraform Cloud API driven Run from GitLab CI/CD
This repo uses GitLab CI/CD to deploy a compute instance in Google Cloud Platform. A Run is created in Terraform Cloud (TFC) from GitLab CI/CD. For the CLI Driven run version using "remote" backend, please see the [remote_backend branch](https://gitlab.com/kawsark/gl-terraform-gcp-compute-test/tree/remote_backend) of this repo.

By default, this configuration provisions a compute instance from image `ubuntu-os-cloud/ubuntu-1804-lts` with machine type `n1-standard-1` in the `us-east1-a` zone. The image, type, zone, and region can all be changed with Terraform variables.

Pre-requisites:
- TFC account and an API Token
- A GitLab repository
- GCP service account credentials (.json file). Please see [GCP provider reference](https://www.terraform.io/docs/providers/google/provider_reference.html#full-reference) for more information.
- GCP Project where a VM can be built

## GitLab CI/CD setup
Clone this repo to your GitLab account

### Optional - Create a GitLab Runner Docker image
The Docker image used for the GitLab Runner in this demo has already made public in Dockerhub as [gitlab-ruby-curl](https://cloud.docker.com/repository/docker/kawsark/gitlab-ruby-curl). This container image is capable of performing an API driven run using `curl` and `jq`.

You can optionally customize a GitLab Runner image using included example [Dockerfile](scripts/Dockerfile). Below are the steps to create a custom image and upload to a container registry.
```
cd scripts
# Customize Dockerfile as needed
docker build -t <your_docker_username>/gitlab-ruby-curl:0.0.1 .
docker login
docker push <your_docker_username>/gitlab-ruby-curl:0.0.1
```

### Register the GitLab Runner
Register the GitLab Runner using GitLab.com UI as below.
- Click on Settings CI / CD Runners
  - Disable Shared Runners 
  - Install a Runner for this repo either locally, or in GKE. You could also install Runners in both places.
    - Host GitLab Runners on GKE: Please see [gke-runners.md](gke-runners.md) steps.
    - Host GitLab Runners locally: Please see [local-runners.md](local-runners.md) steps.
  
Please see [Install GitLab Runner](https://docs.gitlab.com/runner/install/) documenation to learn more about how to install Runners.

### Set GitLab variables
Please set GitLab variables via API or UI (project name > Settings > CI/CD > Variables).
  1. Transform the Google service account credentials .json file using the steps in [transforming GOOGLE_CREDENTIALS - API](transform_gcp_creds-api.md).
  1. Set transformed `GOOGLE_CREDENTIALS`
  1. Set `TFC_ADDR` to `app.terraform.io`, or your Private Terraform Enterprise Server hostname.
  1. Set `TFC_WORKSPACE` to your Terraform Cloud Workspace name
  1. Set `TFC_TOKEN` as your TFC API token. Note: this should be a User token, or a Team token with admin permission on the TFC Workspace (see [API Tokens](https://www.terraform.io/docs/cloud/users-teams-organizations/api-tokens.html#access-levels) for more information).
  1. Set `TFC_ORG` to your Terraform Cloud Organization name
  1. Set `gcp_project` to your GCP project name
  1. Set `gcp_region` to desired GCP region
  

### Trigger GitLab CI/CD Pipeline
Trigger the GitLab CI/CD Pipeline as below.
  1. Push a commit of your forked repo
  1. Or, trigger build by using the terraform remote backend, a merge to the master branch or using a GitLab CI/CD Trigger.
  - GitLab CI/CD Trigger: Obtain a token from Settings > Pipeline triggers (see [adding a new trigger token](https://docs.gitlab.com/ee/ci/triggers/#adding-a-new-trigger) for more details).
  ```
  # Obtain the TOKEN, REF_NAME and project URL from GitLab
  # Example trigger
  curl -X POST \
     -F token=$TOKEN \
     -F ref=$REF_NAME \
     https://gitlab.com/api/v4/projects/7376038/trigger/pipeline
  ```

## Other ideas
1. Divide up the workflow between SecOps and Developer
1. Setup a Slack and/or webhook notification to keep tabs on TFC Run status
1. Use Vault to retrieve temporary GCP credentials

## Testing locally
To test locally, please install terraform 0.12 and use the following example commands. 
- Note: The GOOGLE_CREDENTIALS environment variable must be transformed to remove new lines. Please see [transforming GOOGLE_CREDENTIALS - Local](transform_gcp_creds-local.md).
```
export TF_VAR_gcp_project="your-project-name"
export GOOGLE_CREDENTIALS="$(cat /path/to/gcp-service-account.json)"
terraform init
terraform apply
```
- To destroy, please run the command: `terraform destroy`.
