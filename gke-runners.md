## Setting up GitLab Runners on GKE
Reference: [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)

### Install helm charts
- [Install Helm for your system](https://helm.sh/docs/using_helm/#install-helm)
- Initialize Helm and Install Tiller in current kubectl context (`kubectl config current-context`)
  Reference: [https://helm.sh/docs/using_helm/#initialize-helm-and-install-tiller](initialize-helm-and-install-tiller)
```
helm init --service-account tiller --history-max 200 --upgrade
```
- Alternatively, if you have already installed helm and want to upgrade it please run the command below.
```
helm init --service-account tiller --upgrade
# Verify that the tiller-deploy pod is running
kubectl -n kube-system get po | grep tiller
```


### Download and install the helm chart
Please adjust [my-values.yaml](helm/my-values.yaml) file in this repo as needed.
  - Adjust `runnerRegistrationToken` from GitLab CI / CD
  - Adjust the `image` key if you built and publised your own Docker container
  - Ensure the `tags` key has `curl` and any other `tags` in `.gitlab-ci.yml`.
```
cd helm/
git clone git@gitlab.com:charts/gitlab-runner.git
cd gitlab-runner/
kubectl apply -f ns.yaml 
kubectl apply -f rbac.yaml
# Copy the example my-values.yaml file
cp my-values.yaml gitlab-runner/
helm install --namespace gitlab --name gitlab-runner-1 -f my-values.yaml gitlab-runner
helm ls
```
Note: if you are getting RBAC issues during helm deployment, please consult this excellent guide [Configure RBAC for Helm](https://docs.bitnami.com/kubernetes/how-to/configure-rbac-in-your-kubernetes-cluster/#use-case-2-enable-helm-in-your-cluster).

### Check pods deployed
```
export ns="gitlab"
kubectl get pods -n ${ns}
kubectl logs -f <podname> -n ${ns}
```